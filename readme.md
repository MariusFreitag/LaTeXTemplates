# LaTeXTemplate

## Environment Setup
* Either install [Docker](https://docs.docker.com/install/) or install [TeX Live](https://www.tug.org/texlive/acquire-iso.html) manually
* Install [Visual Studio Code](https://code.visualstudio.com/) with the packages [LaTeX Workshop](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop) and [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
  * To enable German spellchecking, also install [German - Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-german) and add the following line to your [Visual Studio Code Settings](https://code.visualstudio.com/docs/getstarted/settings):
    ``` javascript
    // Set spellcheck language to German
    "cSpell.language": "de",
    ```
  * To hide automatically generated files, add the following lines:
    ``` javascript
    // Exclude auxiliary files
    "files.exclude": {
      "**/.git": true,
      "**/*.aux": true,
      "**/*.bbl": true,
      "**/*.bcf": true,
      "**/*.blg": true,
      "**/*.fdb_latexmk": true,
      "**/*.fls": true,
      "**/*.lof": true,
      "**/*.log": true,
      "**/*.lot": true,
      "**/*.lol": true,
      "**/*.out": true,
      "**/*.run.xml": true,
      "**/*.synctex(busy)": true,
      "**/*.synctex.gz": true,
      "**/*.toc": true,
      "**/*.xdv": true,
      "_minted-*": true
    },
    ```
  * To ensure the correct indentation and new line format, add the following lines:
    ``` javascript
    // Use Linux line endings
    "files.eol": "\n",
    // Insert a new line at the end of each file
    "files.insertFinalNewline": true,
    // Set the tab size to two spaces
    "editor.tabSize": 2,
    // Do not automatically detect the indentation
    "editor.detectIndentation": false,
    // Render spaces which are longer than one character
    "editor.renderWhitespace": "boundary",
    ```
  * If using Docker, create a docker image using the following `Dockerfile`:
    ``` Dockerfile
    FROM ubuntu:latest

    # https://tex.stackexchange.com/a/442652
    RUN ln -snf /usr/share/zoneinfo/Etc/UTC /etc/localtime \
        && echo "Etc/UTC" > /etc/timezone \
        && apt-get update \
        && apt-get upgrade -y \
        && apt-get install texlive-full -y \
        && rm -rf /var/lib/apt/lists/*

    RUN apt-get update && \
        apt-get install python3-pip -y && \
        rm -rf /var/lib/apt/lists/*
    RUN pip install pygments

    RUN apt-get update && \
        apt-get install wget -y && \
        rm -rf /var/lib/apt/lists/*
    RUN wget -O pandoc.deb https://github.com/jgm/pandoc/releases/download/2.7.3/pandoc-2.7.3-1-amd64.deb && \
        dpkg -i pandoc.deb && \
        rm -f pandoc.deb
    ```
  * Configure LaTeX Workshop by inserting the following lines:
    ``` javascript
    // Allow LaTeX linting
    "latex-workshop.chktex.enabled": true,
    // Extend LaTeX build command
    "latex-workshop.latex.recipe.default": "latexmk",
    "latex-workshop.latex.recipes": [
      {
        "name": "latexmk",
        "tools": [
          "latexmk"
        ]
      },
    ],
    "latex-workshop.latex.tools": [
      {
        "name": "latexmk",
        "command": "latexmk",
        "args": [
          "-cd",
          "-synctex=1",
          "-interaction=nonstopmode",
          "-file-line-error",
          "-pdf",
          "-shell-escape",
          "-xelatex",
          "%DOC%"
        ]
      }
    ],
    // Enable sorting of BibTeX files
    "latex-workshop.bibtex-format.sort.enabled": true,
    "latex-workshop.bibtex-format.trailingComma": true,
    ```
  * If using Docker, the following lines must be added:
    ``` javascript
    // Enable docker
    "latex-workshop.docker.enabled": true,
    // Configure docker image
    "latex-workshop.docker.image.latex": "{docker-image}",
    ```
* Open the folder in Visual Studio Code and start writing
* If using Docker, you can add LaTeX tools to your environment by adding aliases:
  * On Mac: `alias latexmk="docker run -i --rm -w /data -v '$PWD':/data {docker-image} latexmk $@"`
  * On Windows: Create a similar script and add its directory to the `PATH` environment variable
* Helpful commands:
  * Build the document manually: `latexmk -pdf -shell-escape -xelatex -halt-on-error`
  * Clean up auxiliary files created in the building process: `latexmk -c`
  * Clean up all generated files: `latexmk -C`

## Repository Structure
### Technical Components
* `.vscode/settings.json`: Contains the recommended Visual Studio Code settings.
* `document.tex`: The LaTeX root file of this document. It includes all other files and syntactically starts the document.
* `document/configuration.tex`: Contains general document configuration.
* `document/titlepage.tex`: Custom first page of the document.
* `document/tools.tex`: Contains custom commands to boost productivity.

### Document Metadata
* `document/docinfo.tex`: Contains metadata and general information about the document and its author.
* `document/language.tex`: Contains language-specific phrases. To translate the document, translating this will be sufficient.

### Content
* `document/abstract.tex`: Abstract of the document.
* `document/acronyms.tex`: Definition of acronyms.
* `document/appendix.tex`: Appendix of the document.
* `document/bibliography.bib`: Declares literature sources and references.
* `document/content.tex`: Main content file. It is recommended to split the content into different files.
* `document/legal.tex`: File to include legal disclaimers or notes.
* `document/embeddings`: Folder to include embeddings like images or other documents.
